﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MF_Translation;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class TextMeshProUGUI_MFT : MonoBehaviour
{
	public int TextId;
	public TranslationSheetEnum Sheet;
	private TextMeshProUGUI Text;
	public bool UpdateByFrame;

	void Awake()
	{
		Text = GetComponent<TextMeshProUGUI>();
		Text.text = LanguageManager.GetText((GeneralTranslationSheetEnum)Sheet, TextId);
	}

	void Update()
	{
		if (UpdateByFrame)
			Text.text = LanguageManager.GetText((GeneralTranslationSheetEnum)Sheet, TextId);
	}

	public void SetText(string text)
	{
		Text.text = text;
	}
}