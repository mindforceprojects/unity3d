﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MF_Translation;

[RequireComponent(typeof(Text))]
public class TextMFT  : MonoBehaviour
{
	public int TextId;
	public TranslationSheetEnum Sheet;
	private Text Text;
	public bool UpdateByFrame;
	
	void Awake()
	{
		Text = GetComponent<Text>();
		Text.text = LanguageManager.GetText((GeneralTranslationSheetEnum)Sheet, TextId);
	}

	void Update()
	{
		if (UpdateByFrame)
			Text.text = LanguageManager.GetText((GeneralTranslationSheetEnum)Sheet, TextId);
	}

	public void SetText(string text)
	{
		Text.text = text;
	}
}
