﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MF_Translation
{
	[Serializable]
	public class TranslationConfig
	{
		public List<Language> Languages;
		public List<Sheet> Sheets;
	}
}

