﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MF_Translation
{
	[Serializable]
	public class SheetText
	{
		public List<TranslationText> Texts;
	}
}