﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MF_Translation
{
	public static class LanguageManager
	{
		private static Dictionary<int, Dictionary<int, string>> Texts;
		public static TranslationConfig TranslationConfig;
		public static Language CurrentLanguage;

		/// <summary>
		///Guarda los idiomas cargados y sus traducciones asociadas.
		///Para eliminar el proceso carga continua de archivos anteriormente leídos se almacenan aquí.
		///En teoría siempre debería tener una y sola una carga siempre y nada más.
		///¿Quién andaría cambiando de lenguaje a cada segundo?
		/// </summary>
		private static Dictionary<string, Dictionary<int, Dictionary<int, string>>> Book;

		static LanguageManager()
		{
			Texts = new Dictionary<int, Dictionary<int, string>>();
			Book = new Dictionary<string, Dictionary<int, Dictionary<int, string>>>();

			TextAsset map = Resources.Load<TextAsset>("Languages - MFT/config");
			TranslationConfig = JsonUtility.FromJson<TranslationConfig>(map.text);

			Load();
		}

		public static void Load(string languague = null)
		{
			if (languague == null)
				languague = Enum.GetName(typeof(SystemLanguage), Application.systemLanguage).ToLower();
			else
				languague = languague.ToLower();			
			
			CurrentLanguage = TranslationConfig.Languages.FirstOrDefault(p => p.Name.ToLower().Contains(languague));

			//Si no encuentra el lenguaje intentará poner por defecto el Idioma Ingles
			//Suponiendo que se encuentre
			if (CurrentLanguage == null)
			{
				CurrentLanguage = TranslationConfig.Languages.FirstOrDefault(p => p.Name.Contains(Enum.GetName(typeof(SystemLanguage), SystemLanguage.English).ToLower()));

				//Si aún así no se encuentra se asigna entonces el primer Idioma de la lista
				if (CurrentLanguage == null)
					CurrentLanguage = TranslationConfig.Languages.FirstOrDefault();
			}

			try
			{
				if (CurrentLanguage == null)
					throw new Exception("Language not found.");
			}
			catch (Exception)
			{
				return;
			}

			if (Book.ContainsKey(languague))
			{
				Texts = Book[languague];
				return;
			}
			
			Texts = new Dictionary<int, Dictionary<int, string>>();
			Book.Add(languague, Texts);

			foreach (var item in TranslationConfig.Sheets)
			{
				var map = Resources.Load<TextAsset>("Languages - MFT/" + CurrentLanguage.Folder + "/" + item.Name.Replace(".json", ""));

				try
				{
					if (map == null)
						throw new Exception("File not found. Folder:" + CurrentLanguage.Folder + " File:" + item.Name);
				}
				catch (Exception)
				{
					continue;
				}

				var sheetText = JsonUtility.FromJson<SheetText>(map.text);

				var dictionaryTexts = new Dictionary<int, string>();
				Texts.Add(item.Id, dictionaryTexts);

				foreach (var text in sheetText.Texts)
					dictionaryTexts.Add(text.Id, text.Text);
			}
		}

		public static void Load(SystemLanguage languague)
		{
			Load(Enum.GetName(typeof(SystemLanguage), languague).ToLower());
		}

		public static string GetText(GeneralTranslationSheetEnum sheet, int text_id)
		{
			try
			{
				return Texts[(int)sheet][text_id];
			}
			catch (Exception)
			{
				return "Error - Sheet:" + sheet + " Row" + text_id;
			}
		}
	}	
}
